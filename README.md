# Adaptador SNES para Arduino

## Descrição

O projeto consiste em utilizar uma interface em Arduino que permita a utilização
de um controle de Super Nintendo como um Joystick genérico de computador. Ele
pode ser utilizado em emuladores para jogar jogos de Super Nintendo, ou até mesmo
em jogos modernos.

## Componentes

| Quantidade |         Componente          |    Valor    |
| :--------- |:--------------------------: | ----------: |
| 1x         | Arduino Leonardo            | emprestado  |
| 5x         | Jumper wire                 | emprestado  |
| 1x         | Conector fêmea p/ SNES      | R$7.00 cada |
|            |         TOTAL               | R$7.00      |

## Circuito

![alt text][circuit]

[circuit]: ./circuito.png

## Imagens do circuito montado

![alt text][foto1]

[foto1]: ./foto1.jpeg

![alt text][foto2]

[foto2]: ./foto2.jpeg

[Vídeo do projeto funcionando](https://youtu.be/61d4Sn8N-m8)

## Explicação do projeto

O controle de Super Nintendo precisa de 3 pinos para dados, 1 pino para 5V e 1
pino para ground. Os três pinos de dados são chamados de data latch, data clock e
serial data.

O seu funcionamento é bem simples. A cada 16ms, o Arduino manda um sinal HIGH no pino de 
data latch para que o controle de Super Nintendo comece a enviar os dados necessários.
Depois disso, o controle manda 16 sinais em ordem, através do pino de serial data, para
cada vez que o arduino aciona o sinal do pino data clock como LOW. O pino serial data
será acionado como LOW pelo controle caso um botão seja pressionado e HIGH caso ele
não seja pressionado.

O pino de serial data deve ser conectado no pino 5 do Arduino, o pino de data latch
deve ser conectado no pino 6 do Arduino e o pino de data clock deve ser conectado
no pino 7 do Arduino.

## Dependências

O programa em Arduino necessita da biblioteca [Arduino Joystick Library](https://github.com/MHeironimus/ArduinoJoystickLibrary).
Instale-a para compilar o programa na Arduino IDE.

## Alunos:

Gabriel Martins Monteiro, NUSP: 14572099 <br>
Eduardo Pereira de Luna Freire, NUSP: 14567304 <br>
Francyelio de Jesus Campos Lima, NUSP: 13676537 <br>
Hélio Márcio Cabral Santos, NUSP: 14577862 <br>


