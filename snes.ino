#include "Joystick.h"

// enumeração para identificar cada botão pressionado
enum {
  SNES_BUTTON_B = 0,
  SNES_BUTTON_Y,
  SNES_BUTTON_SELECT,
  SNES_BUTTON_START,
  SNES_BUTTON_UP,
  SNES_BUTTON_DOWN,
  SNES_BUTTON_LEFT,
  SNES_BUTTON_RIGHT,
  SNES_BUTTON_A,
  SNES_BUTTON_X,
  SNES_BUTTON_L,
  SNES_BUTTON_R,
  NUM_BUTTON
} buttons;

bool button_pressed[NUM_BUTTON];

// os pinos de cada dado do controle
const int data_clock = 7;
const int data_latch = 6;
const int serial_data = 5;

void setup() {
  Serial.begin(9600);
  Joystick.begin();
  // os pinos de data_clock e data_latch serão pinos de escrita
  // enquanto que o pino de serial_data será apenas de leitura
  pinMode(data_clock, OUTPUT);
  pinMode(data_latch, OUTPUT);
  pinMode(serial_data, INPUT);
}

void loop() {
  // começa-se escrevendo o pino de data_latch como HIGH, para começar o ciclo
  digitalWrite(data_latch, HIGH);
  delayMicroseconds(12);
  digitalWrite(data_latch, LOW);
  delayMicroseconds(6);

  int i = 0;

  while (i < 16) {
    // o ciclo de cada botão deve durar 12 microsegundos. A primeira parte, o pino de data_clock deverá ser LOW.
    // Na segunda parte, o pino de data_clock deverá ser HIGH, para que o controle de super nintendo avance
    // para a informação do próximo botão
    digitalWrite(data_clock, LOW);
    delayMicroseconds(6);

    if (i < NUM_BUTTON) {
      // caso o pino de serial_data seja LOW, o botão correspondente foi pressionado.
      if (digitalRead(serial_data) == LOW) button_pressed[i] = true;
      else button_pressed[i] = false;
    }

    digitalWrite(data_clock, HIGH);
    delayMicroseconds(6);
    i++;
  }

  for (int i = 0; i < NUM_BUTTON; i++) {
    if (button_pressed[i]) Joystick.pressButton(i);
    else Joystick.releaseButton(i);
  }
}
